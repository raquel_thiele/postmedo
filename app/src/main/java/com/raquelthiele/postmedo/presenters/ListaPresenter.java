package com.raquelthiele.postmedo.presenters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.raquelthiele.postmedo.models.Post;
import com.raquelthiele.postmedo.services.ServicoPost;
import com.raquelthiele.postmedo.views.ListaActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Classe responsavel por recuperar a lista de Posts e retorna-la formatada para a view ListaActivity, assim
 * como decidir acoes de acordo com interacoes com o usuario.
 */

public class ListaPresenter {

    ListaActivity mView;
    ServicoPost mPost;

    public ListaPresenter(ListaActivity view, ServicoPost post) {

        mView = view;
        mPost = post;
    }

    public void carregaPosts() {
        final String userId = "5";
        Call<List<Post>> call = mPost.getApi().getPostByUserId(userId);


        try {
            call.enqueue(new Callback<List<Post>>() {
                @Override
                public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                    mView.exibePosts(response.body());
                }

                @Override
                public void onFailure(Call<List<Post>> call, Throwable t) {
                    Log.e(this.getClass().getSimpleName(), "Ocorreu um erro ao exibir os posts.");
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void criaPost() {
        final String userId = "5";
        Call<Post> call = mPost.getApi().createPost("Post da Raquel", "Post Me Do é lindo, gente!", userId);

        try {
            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {
                    AlertDialog alertDialog = new AlertDialog.Builder(mView).create();
                    Post postCriado;
                    postCriado = response.body();
                    alertDialog.setTitle("Post criado com sucesso");
                    alertDialog.setMessage("Seu post foi criado com sucesso!" + postCriado.toString());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    mView.onResponse();
                }

                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    Log.e(this.getClass().getSimpleName(), "Ocorreu um erro ao criar o post.");
                    mView.onErrorResponse();
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
