package com.raquelthiele.postmedo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raquelthiele.postmedo.R;
import com.raquelthiele.postmedo.models.Post;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adaptador customizado que carregara a lista de Posts e a mostrara na ListView de activity_lista.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private final Context contexto;
    private final ArrayList<Post> posts;

    public PostsAdapter(Context contexto, ArrayList<Post> posts) {
        this.contexto = contexto;
        this.posts = posts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(contexto).inflate(R.layout.layout_post_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = posts.get(position);
        holder.postId.setText("Post #" + post.getId());
        holder.userId.setText("From user #" + post.getUserId());
        holder.title.setText(post.getTitle());
        holder.body.setText(post.getBody());
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void addPosts(List<Post> posts) {
        int positionStart = this.posts.size();
        int itemCount = posts.size();
        this.posts.addAll(posts);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void clear() {
        int itemCount = posts.size();
        posts.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.post_id)
        TextView postId;
        @BindView(R.id.user_id)
        TextView userId;
        @BindView(R.id.post_title)
        TextView title;
        @BindView(R.id.post_body)
        TextView body;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
