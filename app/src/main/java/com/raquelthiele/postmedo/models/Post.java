package com.raquelthiele.postmedo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe POJO gerada a partir do arquivo JSON do JSONPlaceHolder
 */

public class Post {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;

    public Post() {
    }

    /**
     * @param id
     * @param body
     * @param title
     * @param userId
     */
    public Post(int userId, int id, String title, String body) {
        super();
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    /**
     * @return O userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId O userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return O id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id O id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return O title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title O title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return O body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body O body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * Sobrecarga do metodo toString() para melhor visualizacao do objeto do post
     *
     * @return
     */
    @Override
    public String toString() {
        String pularLinha = "\n";
        return pularLinha + pularLinha + "Id gerado: " + this.getId() + pularLinha + "Id do usuario: " + this.getUserId() +
                pularLinha + "Título: " + this.getTitle() + pularLinha + "Corpo do texto: " + this.getBody() + pularLinha;
    }
}


