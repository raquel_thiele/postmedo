package com.raquelthiele.postmedo.views;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.raquelthiele.postmedo.R;
import com.raquelthiele.postmedo.adapters.PostsAdapter;
import com.raquelthiele.postmedo.models.Post;
import com.raquelthiele.postmedo.presenters.ListaPresenter;
import com.raquelthiele.postmedo.services.ServicoPost;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 */
public class ListaActivity extends AppCompatActivity
        implements View.OnClickListener {

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.fabdelete)
    FloatingActionButton mFabDelete;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private PostsAdapter mPostsAdapter;
    private ListaPresenter mListaPresenter;
    private ServicoPost mServicoPost;
    private Snackbar loadingSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        ArrayList<Post> postsVazios = new ArrayList<>();
        mPostsAdapter = new PostsAdapter(this, postsVazios);
        mRecyclerView.setAdapter(mPostsAdapter);

        mServicoPost = new ServicoPost();
        mListaPresenter = new ListaPresenter(this, mServicoPost);
        mListaPresenter.carregaPosts();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.fab)
    public void onClick(View view) {
        mFab.hide();
        mFabDelete.hide();
        if (loadingSnackbar == null) {
            loadingSnackbar = Snackbar.make(view, "Criando...",
                    Snackbar.LENGTH_INDEFINITE);
        }


        loadingSnackbar.show();
        mListaPresenter.criaPost();
    }

    public void exibePosts(List<Post> posts) {
        mPostsAdapter.clear();
        mPostsAdapter.addPosts(posts);
    }

    public void onResponse() {
        loadingSnackbar.dismiss();
        mFab.show();
        mFabDelete.show();
    }

    public void onErrorResponse() {
        loadingSnackbar.dismiss();
        mFab.show();
        Snackbar.make(mRecyclerView, "Ocorreu um erro!", Snackbar.LENGTH_LONG)
                .setAction("Ok", this)
                .show();
    }
}