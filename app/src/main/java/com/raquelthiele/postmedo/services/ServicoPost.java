package com.raquelthiele.postmedo.services;

import com.raquelthiele.postmedo.models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interface de serviço criada para as chamadas de serviço utilizando o Retrofit.
 */
public class ServicoPost {

    private static final String POST_SERVER_URL = "http://jsonplaceholder.typicode.com";
    private PostApi postApi;

    public ServicoPost() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(POST_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        postApi = retrofit.create(PostApi.class);
    }

    public PostApi getApi() {

        return postApi;
    }

    public interface PostApi {
        /**
         * Método para obter todos os posts existentes
         *
         * @return todos os posts
         */
        @GET("/posts")
        Call<List<Post>> getAllPosts();

        /**
         * Obter posts a partir de um userId.
         *
         * @param userId - UserId criador dos posts
         * @return posts de um determinado userId
         */
        @GET("/posts")
        Call<List<Post>> getPostByUserId(@Query("userId") String userId);

        /**
         * Obter um post especifico a partir de um id.
         *
         * @param id - Id do post
         * @return um determinado post
         */
        @GET("/posts/{id}")
        Call<Post> getPost(@Path("id") int id);

        /**
         * Criar um post de um determinado user com titulo e corpo do texto.
         *
         * @param title  - titulo do post
         * @param body   - conteudo do post
         * @param userId - id do usuario criador do post
         * @return Post criado contendo o seu Id
         */
        @FormUrlEncoded
        @POST("/posts")
        Call<Post> createPost(@Field("title") String title, @Field("body") String body, @Field("userId") String userId);

        /**
         * Deletar posts de um userId
         *
         * @param userId - id do usuario para exclusao dos posts
         * @return Resultado da exclusão
         */
        @DELETE("/posts/{userId}")
        Call<Post> deletePost(@Path("userId") String userId);
    }
}
